<img style="float:left;" src="images/IDSNlogo.png" width="200" height="200"/>

## Hands on Assignment: Classify Drugs using a Data Modeler (30min)

According to Forbes, billions of dollars will be poured into AI Drug Development. In this hands-on exercise, you will be acting as a researcher in a pharmaceutical startup who has been tasked with utilizing AI to predict appropriate drugs for patient treatment.

You have been provided with data from a drug study where different patients were treated with different drug formulations, and their response to those drugs was collected. The data also includes health metrics for each patient, such as their age, sex, cholesterol, blood Sodium, and Potassium values, etc.

Armed with this data, you can leverage the power of AI to build a model that predicts which drug formulation is most suitable for a future patient with the same illness.

You will build the model using IBM Watson Studio. The Watson Studio Data Modeler is a service that allows us to interactively create, train, and deploy machine learning models. Follow the steps below to complete this exercise.

NOTE: For learners working to get a certificate for this course (i.e. not auditing), please go through this exercise carefully and thoroughly, as it will be used for the graded assessment that follows.

FYI: In order to complete this exercise, you will be creating an IBM Cloud account and provisioning an instance for Watson Studio Service. A credit card is NOT required to sign up for IBM Cloud Lite account and there is no charge associated in creating a Lite plan instance of the Watson Studio service

## Step 1: Create an IBM Cloud Account

1. Go to: [Create a free account on IBM Cloud](https://cloud.ibm.com/registration?cm_mmc=Email_Outbound-_-Developer_Ed+Tech-_-WW_WW-_-Campaign-Skills+Network+Cognitive+Class+edX+AI0101EN+&cm_mmca1=000026UJ&cm_mmca2=10006555&cm_mmca3=M12345678)

2. In the Email box, enter your email address and then click the arrow.

<img src="images/13.jpg"  width="600">

3. When your email address is accepted, enter your **First Name**, **Last Name**, **Country or Region**, and create a **Password**.

**Note:** To get enhanced benefits, please sign up with your company email address rather than a free email ID like Gmail, Hotmail, etc.

If you would like IBM to contact you for any changes to services or new offerings, then check the box to accept the option to be notified by email.

4. Click **Create Account** to create your IBM Cloud account.

# Task 2: Confirm your email address

1. An email is sent to the address that you signed up with.

<img src="images/14.jpg"  width="600">

2. Check your email, and in the email that was sent to you, click **Confirm Account**.

<img src="images/15.jpg"  width="600">

3. You will receive notification that your account is confirmed.

<img src="images/16.jpg"  width="600">

Click **Log In**, and you will be directed to the IBM Cloud Login Page

# Task 3: Login to your IBM Cloud account

1. On the [Log in to IBM Cloud](https://cloud.ibm.com/login)page, in the ID box, enter your email address and then click **Continue**.

<img src="images/17.jpg"  width="600">

2. In the **Password** box, enter your password, and then click **Log in**.

<img src="images/18.jpg"  width="600">

## Step 2: Create a Watson Studio Resource Scenario

To manage all your projects, you will use IBM Watson Studio. In this exercise, you will add Watson Studio as a Resource.

## Task 1: Add Watson Studio as a resource

1. On the Dashboard, click **Create Resource**.

<img src="images/19.jpg"  width="600">

2. In the Catalog, click **AI (16)**.

<img src="images/20.jpg"  width="600">

Note that the **Lite** Pricing plan is selected.

3. In the list of **Services**, click **Watson Studio**.

<img src="images/21.jpg"  width="600">

4. On the Watson Studio page, select the region closest to you, verify that the **Lite** plan is selected, and then click **Create**.

<img src="images/22.jpg"  width="600">

5. When the Watson Studio resource is successfully created, you will see the Watson Studio page. Click **Get Started**.

<img src="images/23.jpg"  width="600">

6. You will see this message when Watson Studio is successfully set up for you.

<img src="images/24.jpg"  width="600">

## STEP 3: Create a Project

To create a Watson Machine Learning instance, click on **Create a Project** and then click on **Create an Empty Project**.

<img src="images/25.jpg"  width="600">

Now let's fill in some project details and click **Create**. The IBM Cloud Object Storage, which provides you storage for your images, should be automatically created for you.

<img src="images/26.jpg"  width="600">

After creating your project, by default, you will land on the **Projects** page of your project. Click **Add to Project**.

When the **Choose Asset Type** box pops up select **Modeler flow** in the list

<img src="images/27.jpg"  width="600">

<img src="images/28.jpg"  width="600">

On the next screen, choose **From Example**, then choose **Drug Study Example** from the options

## STEP 4: Explore Drug Study Model

After creating your project, by default, you will land on the page where you can view, and explore the drug study model. Normally you would create this model from scratch but in this case an example model flow is already provided.

The model *flow* is a visual representation of the path the data will take and what decisions will be made at each node in order to determine which drug is appropriate for a given patient.

This modeler flow reads training data (represented by the purple node on the left). Then, in the next node computes the Sodium (Na) to Potassium (K) ratio for each patient in the dataset, discards the sodium and potassium columns from the dataset, and defines the target column. After that it utilizes two different algorithms for predicting the results – a Neural Network (top) and C5.0 Decision Tree (bottom).

<img src="images/29.jpg"  width="600">

Right click the purple box, labeled **DRUG1n**, and choose **Preview**.

The preview button shows data set before you actually run it through the model. This is the training data. Notice there is no prediction about whether the drug chosen is the right one. We’ll get the result once we run the model.

<img src="images/30.jpg"  width="600">

## STEP 5: Run Drug Study Model

Now that you’ve explored the model and have begun to understand the underlying data and algorithm, we will run the model to view and analyze the outputs.

Towards the top of the screen, press the **Play** button (<img src="images/playback.jpg"  width="20">), the model should render.

## STEP 6: Check Out The Results!

Once you have run the model, Watson Machine Learning will create new data sets using the two algorithms specified in the model flow to predict which drug is best for each patient. Beside which drug it recommends for each patient, it will also give you a confidence score (between 0 and 1) on how confident it thinks the recommendation is (0 for lowest confidence and 1 for highest confidence).

Right click the top orange box called **Drug**, and click **Preview**

<img src="images/31.jpg"  width="600">

Notice the fields labeled **$N-Drug**, and **$NC-Drug**. These fields are computed by the neural network algorithm and represent predicted drug value and the confidence score.

<img src="images/32.jpg"  width="600">

Now Right click the bottom orange box called **Drug**, and click **Preview**

<img src="images/33.jpg"  width="600">

Note the fields labeled **$C-Drug**, and **$CC-Drug**. These are the predicted values and confidence scores using the C5 decision tree algorithm. Notice the change in confidence score in the $CC-Drug column.

<img src="images/34.jpg"  width="600">

You can also right click each model, and choose **View Model** to explore information about each of the models and their outputs.

## STEP 7: Save A Screenshot

From Step 6 take and save screenshots in .jpeg or .jpg format of the outputs of both models. Ensure the labels and confidence scores (the $CC-Drug and $NC-Drug columns) in the table are readable. See a sample screenshot above.

Answer the question: For the first patient (Age 23 F) which model did a better job of predicting the suitable drug for treatment?

**Hint:** Consider the confidence scores

## STEP 8: Share your Results!

<img src="images/35.jpg"  width="600">

[Click here to share the above Tweet.](https://twitter.com/intent/tweet?text=I%20just%20learned%20how%20to%20use%20Artificial%20Intelligence%20to%20predict%20drug%20suitability%20for%20patient%20treatment%20with%20@IBMWatson%20in%20this%20AI%20for%20Everyone%20course%20by%20@ravahuja%20on%20@edXonline%20-%20https://www.edx.org/course/artificial-intelligence-for-everyone)

















