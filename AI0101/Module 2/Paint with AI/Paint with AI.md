<img style="float:left;" src="images/IDSNlogo.png" width="200" height="200"/>

## Hands-On Lab: Paint with AI (20 mins)

# Paint with AI

IBM Research creates innovative tools and resources to help unleash the power of AI. In this hands-on lab, you will use a new kind of neural network, called a generative adversarial network (GAN), to create complex outputs, like photorealistic images. You will use a GAN to enhance existing images and create your own unique, custom image.

### Follow these steps to work with a GAN:

1. Access the demo here:[This landscape image was created by AI](https://gan-paint-demo.mybluemix.net/?cm_mc_uid=18442949964915524319331&cm_mc_sid_50200000=60398911561414360866&cm_mc_sid_52640000=69930581561414360872)

2. In the **Co-create with a neural network** section, under **Choose a generated image**, select one of the existing images. For example, choose the 11th image.

3. From the Pick object type list, select the type of object you want to add. For example, click on **Tree** to select it.

*Figure 1 - Original generated image*

<img src="images/1.png"  width="600">


4. Move the cursor onto the image. Click and keeping the mouse button pressed, drag your cursor over an area of the existing image where you want to add the object. For example drag a line in the red area highlighted in the red rectangle below to add a tree there.

<img src="images/2.png"  width="600">


5. Choose another object type and add it to the image.

*Figure 2 - Trees and grass added:*

<img src="images/3.png"  width="600">


6. Experiment with locations: Can you place a door in the sky? Can you place grass so that it enhances the appearance of existing grass? 7. Use the **Undo** and **Erase** functions to remove objects. 8. [Optional] Click **Download** to save your work.

**For more information on the capabilities of GANs, follow these steps:**

1. In the **What’s happening in this demo** section? Click What does a GAN “understand” and read the text.

2. What does the text say about placement of objects? Does this explain the results you saw earlier?

3. Click **Painting with neurons, not pixels** and read the text. How does the GAN help you manipulate images?

4. Click **New ways to work with AI** and read the text. What are some of the use cases for GANs?

Use the Discussion Forum to talk about these questions with your fellow students

