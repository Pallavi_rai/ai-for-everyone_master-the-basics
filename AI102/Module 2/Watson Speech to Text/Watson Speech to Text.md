<img style="float:left;" src="image/IDSNlogo.png" width="200" height="200"/>

# Hands-on Lab: Watson Speech to Text (10 min)

## Exploring Watson Speech to Text

IBM provides an online demo of Watson Speech to Text at:[Watson Speech to Text](https://speech-to-text-demo.ng.bluemix.net/?cm_mc_uid=59963159489715526067556&cm_mc_sid_50200000=93080491558737820643&cm_mc_sid_52640000=17830021558737820661)(or from the product page, click Demo). 

The demo environment has several ways you can provide audio:

- Record Audio - uses your microphone to record audio.

- Upload Audio File - allows you to upload an .mp3, .mpeg, .wav, .flac, or .opus format audio file.

- Play a sample file.

In this demo, you will use Watson Speech to Text to transcribe audio.

Use the following steps to explore the demo:

1. Access the online demo here:[Watson Speech to Text](https://speech-to-text-demo.ng.bluemix.net/?cm_mc_uid=59963159489715526067556&cm_mc_sid_50200000=93080491558737820643&cm_mc_sid_52640000=17830021558737820661)

2. Click **Play Sample 1**.

3. Observe as Watson Speech to Text transcribes the audio and detects multiple speakers.

4. Click **Word Timings and Alternatives**.

5. On the first line, hover over **so2**

6. How many alternatives does Watson provide?

7. How confident is Watson that this is the word “so”?

8. In the fourth paragraph, hover over **a2**

9. How confident is Watson that this is the word “a”?

10. What alternatives does Watson provide?

11. In the **Keywords to spot** box, read the suggested keywords.

12. In the transcribed text box, click **Keywords (5/9)**.

13. How many times was “artificial intelligence” spotted?

14. To add a keyword, in the **Keywords to spot** box, click before **IBM** and type *operability*, and then click **Play Sample 1**.

15. Did Watson spot the additional keyword?



Optional (requires microphone)

1. Click to clear the **Detect multiple speakers** check box.

2. In the  **Keywords to spot** box, delete the existing keywords and add the following text: *moon, decade, rocket, easy, hard*

3. Click **Record Audio**.

4. Record the following text: *We choose to go to the moon. We choose to go to the moon in this decade and do the other things, not because they are easy, but because they are hard, because that goal will serve to organize and measure the best of our energies and skills, because that challenge is one that we are willing to accept, one we are unwilling to postpone, and one which we intend to win, and the others, too*.

5. Click **Word Timings and Alternatives**.

6. Does Watson suggest any alternatives for words?

7. What is Watson’s level of confidence for each alternative?

8. Click **Keywords (x/5)**.

9. Did Watson spot the keyword “rocket”?

10. Why not?



































