<img style="float:left;" src="images/IDSNlogo.png" width="200" height="200"/>

# Hands-on Lab: Watson Discovery (10 min)

## Exploring Watson Discovery

IBM provides an online demo of Watson Discovery at [Discovery](https://discovery-news-demo.ng.bluemix.net/?cm_mc_uid=34405386824515579630816&cm_mc_sid_50200000=94903771557963081679&cm_mc_sid_52640000=32922651557963081683) (or from the product page, click View demo).

Use the following steps to explore the demo:

1. Access the online demo here:[Discovery](https://discovery-news-demo.ng.bluemix.net/?cm_mc_uid=34405386824515579630816&cm_mc_sid_50200000=94903771557963081679&cm_mc_sid_52640000=32922651557963081683)

2. Enter the name of a company and click search. Choose a well-known company or one that has been in the news recently.

3. Scroll down to view **Top Stories** and **Top Entities**.

4. Did Discovery find any recent articles? If no stories are found, try searching for a different company.

5. Click **View Query** to see the query code.

6. Click **Response** to see the response code.

7. Click **Go Back** to return to the **Top Stories**.

8. Under **Top Entities**, click **Topics**, **Companies**, and **People** in turn to see the most frequently mentioned entities associated with the company you searched for.

9. Do you recognize any from recent news stories?

10. Do any of the associations seem unusual?

11. Scroll down to view **Sentiment Analysis**.

12. Is the overall reporting on your selected company negative or positive?

13. What is the percentage split?

14. Scroll down to view **Anomaly Detection**.

15. Were any anomalies detected?

16. If so, hover over the anomalous data point – do you recognize the article mentioned?

17. Scroll down to view **Co-Mentions and Trends**.

18. Expand each section to view the trends.

19. Is the overall view negative or positive?
