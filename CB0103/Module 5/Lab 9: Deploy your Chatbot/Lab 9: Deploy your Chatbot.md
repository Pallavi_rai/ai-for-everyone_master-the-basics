<img style="float:left;" src="images/IDSNlogo.png" width="200" height="200"/>

##### Lab 9: Deploy your Chatbot

## Exercise 1: Deploy a chatbot to your website

1. **Log into the WordPress site you just generated**. Visit your *Dashboard URL* and use the
credentials you obtained in the previous lab to log in.

2. **Activate the Watson Assistant plugin**. In the *Plugins* section of your WordPress Dashboard,
you'll find a few plugins that were installed for you. Click on the *Activate* link under the plugin
for Watson.

3. **Click on the link** prompting you to provide your credentials.

<img src="images/11.png"  width="600">

4. Next, **click on the *Plugin Setup* tab**. Here **specify your Watson Assistant credentials** for the
chatbot assistant we created in the previous lab.

```
As a reminder, you’ll just need your Assistant URL and API Key. If you don’t know where to find
them, review the previous lab again as instructions are provided in Lab 8.
```

4. **Make sure that the chatbot is enabled**. In the future, should you decide to temporarily
disable the chatbot, you'll be able to do so from this page by deselecting the checkmark next to
*Enable Chatbot*.

<img src="images/12.png"  width="600">

5. **Click on *Save Changes*** and a message should appear inviting you to Browse your website to
see the chatbot in action at the top. *Click on that link* or simply head over to the Instance URL
you made note of earlier on.

<img src="images/13.png"  width="600">

6. If you see a chatbot pop up greeting you with the prompt you defined, congratulations, **you
just deployed your chatbot**. That was quite straightforward, wasn't it? Go ahead and *test your
chatbot directly through this chat box*.

<img src="images/14.png"  width="600">

## Exercise 2: Customize the chat box window

Now that our chatbot is deployed we can take a moment to celebrate our accomplishment.
Whenever we make changes to the chatbot on Watson Assistant, these changes will be
reflected on our already deployed chatbot. So, we could literally walk away from the WordPress
site now, and just focus on Watson Assistant improvements.

However, before we do so, I'd like for you to spend some time customizing the chatbot look
and feel on the site. Specifically, the look of the chat box.

1. Go back to your WordPress Dashboard. **Click on the *Watson Assistant* link within your
WordPress dashboard** sidebar (towards the bottom of the page).

2. Next, **click on *Customize Plugin* and then the *Chat Box* tab.** 

<img src="images/15.png"  width="600">

Spend some time to customize the chat box. Change some of the options within this tab and
then visit your WordPress site to see how they affect the look of your chat box. For inspiration,
see how I customized it in the image below.

<img src="images/16.png"  width="600">

## Exercise 3: Familiarize yourself with the plugin options

The plugin is divided into three sections: *Setup Chatbot, Customize Plugin*, and *Advanced
Features*. **Take some time to explore these three sections.**

Some of the options are for features we haven't discussed yet or are out of scope for this
introductory course. But it's good to know what options the plugin has to offer and, as always,
if you have any plugin-specific questions feel free to ask them in the forum.


























